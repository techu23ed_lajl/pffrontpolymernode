var express = require('express');
var router = express.Router();
var controllers = require('../controllers/index.js')

router.get('/', controllers.loginController.index);
router.get('/login', controllers.loginController.login);
router.get('/RegistroUsuarios', controllers.loginController.registroLogin);
/*
router.get('/', function(req, res, next){
  console.log("estoy en el router");
  res.render("index", {root:'.'} );
  // res.sendFile(path.join(__dirname, 'index.html'));
});
*/

module.exports = router;
