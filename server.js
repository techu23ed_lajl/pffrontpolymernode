var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
// * Para que el navegador identifique donde se encuentran nuestros componentes web programados con Polymer
app.use( express.static(__dirname + '/build/default') );

app.listen(port);

app.get('*', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  res.sendFile( "index.html", {root:'.'}  );
});
