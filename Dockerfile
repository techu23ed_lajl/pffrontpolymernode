#Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copia de archivos. De mi carpeta del release a la carpeta del contenedor
#ADD ./build/default /app/build/default
#ADD server.js       /app
#ADD package.json    /app
ADD . /app


#Dependencias sólo las que están en el apartado "Dependencies"
RUN npm install

#Puerto que exponemos
EXPOSE 3000

#Comando que requiere mi app para ejecutarse
CMD ["npm", "start"]
